%% for's
%% adjustable parameters %CHECK
% clear all;
folder = 2;                     % path of the results folder
kM = 3;                         % 1.LBP - 2.LTP - 3.LDP - 4.LDN
kindMask = 2;                   % 1.Sobel - 2.Kirsch - 3.Gaussian
sizeMask = 1;                   % 1-10
spacesInMask = 0;               % 0-1-2-3
kProminent = 2;                 % k - prominent 1-2-3-4
grid = 1:3:7;                  % grid[y x] 3:10
gf = 1;                         % gauss factor over the Histogram
totalElem = 2622;               % total elements in the database
vecTrain = 80;                  % percentage of the DB for TRAIN from 1 to 99
times = 5;                      % number of times that SVM will classify (emulating cross-validation)
%% fixed parameters
listMethod = {'LBP','LTP','LDP','LDN'};
listMask = {'Sobel','Kirsch','Gaussian'};
switch(listMask{kindMask})
    case 'Kirsch'
        dirsInMask = 8;
        totalAngle = 360;
    otherwise
        dirsInMask = 4;
        totalAngle = 180;
end 
sizes = 3:2:21; % 3-5-7-9-11-13-15-17-19-21
sigmas = {0.3,0.7,1.1,1.5,2.0,2.5,3.0,3.6,4.2,4.8}; 
if strcmp(listMask{kindMask},'Gaussian')
    sizeMask = sigmas{sizeMask};
else
    sizeMask = sizes(sizeMask);
end
sizeHist = 28;
% gauss factor
% gf = 7;
% ((GaussHist(gf,2)/GaussHist(gf,1))-1)*100;
GaussHist = -1;     % -300
%  GaussHist(1,:) = [0.8 0.2 0.8];     % -300
%  GaussHist(2,:) = [1.0 1.0 1.0];     % same
%  GaussHist(3,:) = [0.5 2.0 0.5];     % 300

% database
PathDB= '/home/vision/Documents/Leo/DB/FERET-norm-rot/';                    %linux
gTruth = 'ground_truth/name_value/';                                        %linux
subDB = {'fa','fb','dupI','dugpII'};
PathRoot = '/home/vision/Documents/Leo/gender-classification/';              %linux
PathResults = strcat(PathRoot,num2str(folder),'.Result/');                  %linux
extension = 'jpg';
fl = load(strcat(PathRoot,'ListNumGender.mat'));
genderList = fl.ListNumGender;
%% ########################## evaluation by SVM ###########################
vecTrain = round(totalElem*(vecTrain/100));
vecTest = vecTrain+1;
ixVector = cell(times,3);
for ixv = 1:times
  vecTotal = randperm(totalElem);
  ixVector{ixv,1} = vecTotal(1:vecTrain);
  ixVector{ixv,2} = vecTotal(vecTest:end);
end
%% for's
 for g = 1:length(grid)
  fileInputSvm = strcat(PathResults,...
        listMethod{kM},...
        '.',listMask{kindMask},...
        '.sizeM',num2str(sizeMask),...
        '.spacesM',num2str(spacesInMask),...
        '.k',num2str(kProminent),...
        '.grid',num2str(grid(g)),...
        '.GHist',num2str(gf),...
        '.file.mat');
        fprintf('%s\n',fileInputSvm);
        vh = load(fileInputSvm);
        Vector = vh.tMatTotal;
  for gc = 4:8
   ACC = zeros(times,1);
   for c = 1:times
    gamma = strcat('1.0e-',num2str(gc));
    cost = strcat('1.0e+',num2str(gc));
    fprintf('Start running SVM from gamma %s and cost %s...\n',gamma,cost);
    svmPar = ['-c ' cost ' -g ' gamma];
    model = misvmtrain(genderList(ixVector{c,1}),Vector(ixVector{c,1},:),svmPar);
    [~, acc, ~] = misvmpredict(genderList(ixVector{c,2}),Vector(ixVector{c,2},:),model);
    ACC(c) = acc(1);
%     refDetails = strcat(PathResults,'ResultsDetails','.txt');
%     fid = fopen(refDetails, 'a');
%     fprintf(fid,'[gamma: %s, cost: %s, mask: %s, sizeM: %d, sizeM: %d, spacesM: %d, k: %d, grid: %d, GaussHist: %d] -> %s \n',...
%                 gamma,cost,typeMask{kindMask},sizeMask,spacesInMask,kProminent,grid,tgH,acc);
%     fprintf(fid,'%s\n',lines{ix(1:end-2)});
%     fclose(fid);

    ref = strcat(PathResults,'Results','.csv');
    fid = fopen(ref, 'a');
    fprintf(fid,'%s,%s,%s,%s,%d,%d,%d,%d,%d,%f \n',...
      gamma,cost,listMethod{kM},listMask{kindMask},sizeMask,spacesInMask,kProminent,grid(g),gf,acc(1));
    fclose(fid);
% 
%             fprintf('End running SVM...\n');
%% end-for's
   end
   ref = strcat(PathResults,'Results','.txt');
   fid = fopen(ref, 'a');
   fprintf(fid,'[gamma: %s, cost: %s, %s, mask: %s, sizeM: %s, spacesM: %d, k: %d, grid: %d, GHist: %d] -> %f \n',...
          gamma,cost,listMethod{kM},listMask{kindMask},num2str(sizeMask),spacesInMask,kProminent,grid(g),gf,mean(ACC(:)));
    fclose(fid);
  end
 end

ref = strcat(PathResults,'Results','.txt');
fid = fopen(ref, 'a');
fprintf(fid,'------------------------------------------------------------------------------------------------------------------------');
fclose(fid);

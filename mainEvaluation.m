%% for's
%% adjustable parameters %CHECK
% clear all;
folder = 2;                     % path of the results folder
kM = 3;                         % 1.LBP - 2.LTP - 3.LDP - 4.LDN
kindMask = 2;                   % 1.Sobel - 2.Kirsch - 3.Gaussian
sizeMask = 1;                   % 1-10
spacesInMask = 0;               % 0-1-2-3
kProminent = 2;                 % k - prominent 1-2-3-4
% grid = 1:3:13;                  % grid[y x] 3:10
grid = 1:3:7;
gH = 1;                         % gauss factor over the Histogram
%% fixed parameters
listMethod = {'LBP','LTP','LDP','LDN'};
listMask = {'Sobel','Kirsch','Gaussian'};
switch(listMask{kindMask})
    case 'Kirsch'
        dirsInMask = 8;
        totalAngle = 360;
    otherwise
        dirsInMask = 4;
        totalAngle = 180;
end 
sizes = 3:2:21; % 3-5-7-9-11-13-15-17-19-21
sigmas = {0.3,0.7,1.1,1.5,2.0,2.5,3.0,3.6,4.2,4.8}; 
if strcmp(listMask{kindMask},'Gaussian')
    sizeMask = sigmas{sizeMask};
else
    sizeMask = sizes(sizeMask);
end
sizeHist = 28;
% gauss factor
% gf = 7;
% ((GaussHist(gf,2)/GaussHist(gf,1))-1)*100;
GaussHist = -1;     % -300
%  GaussHist(1,:) = [0.8 0.2 0.8];     % -300
%  GaussHist(2,:) = [1.0 1.0 1.0];     % same
%  GaussHist(3,:) = [0.5 2.0 0.5];     % 300

% database
PathDB= '/home/vision/Documents/Leo/DB/FERET-norm-rot/';                    %linux
gTruth = 'ground_truth/name_value/';                                        %linux
subDB = {'fa','fb','dupI','dugpII'};
PathRoot = '/home/vision/Documents/Leo/gender-classification/';              %linux
PathResults = strcat(PathRoot,num2str(folder),'.Result/');                  %linux
extension = 'jpg';
%% ############################# execution #############################
%%
fprintf('%s.%s.sizeM%d.spacesM%d.k%d.grid%d.GHist%d \n',...
        listMethod{kM},listMask{kindMask},sizeMask,spacesInMask,kProminent,length(grid),length(GaussHist));
% 
principal(PathDB,...
    gTruth,...
    subDB,...
    PathRoot,...
    PathResults,...
    extension,...
    listMethod{kM},...
    listMask{kindMask},...
    totalAngle,...
    dirsInMask,...
    sizeMask,...
    spacesInMask,...
    kProminent,...
    sizeHist,...
    grid,...
    GaussHist);
%% end-for's
% TestEvaluation
